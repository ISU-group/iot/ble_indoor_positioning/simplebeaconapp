import 'dart:math';

import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter_beacon/flutter_beacon.dart';
import 'package:beacon_app/controller/requirement_state_controller.dart';
import 'package:get/get.dart';

const acceptableAccuracy = 0.2;

class TabScanning extends StatefulWidget {
  @override
  _TabScanningState createState() => _TabScanningState();
}

class _TabScanningState extends State<TabScanning> {
  StreamSubscription<RangingResult>? _streamRanging;
  final _regionBeacons = <Region, List<Beacon>>{};
  final _beacons = <Beacon>[];
  final controller = Get.find<RequirementStateController>();
  final regions = {
    'B9407F30-F5F8-466E-AFF9-25556B57FE6D': 'iBeacon',
  };

  var isUserSeat = false;

  static var beaconIds = [];

  static var userBeaconId = -1;

  @override
  void initState() {
    super.initState();

    controller.startStream.listen((flag) {
      if (flag == true) {
        initScanBeacon();
      }
    });

    controller.pauseStream.listen((flag) {
      if (flag == true) {
        pauseScanBeacon();
      }
    });
  }

  initScanBeacon() async {
    await flutterBeacon.initializeScanning;
    if (!controller.authorizationStatusOk ||
        !controller.bluetoothEnabled) {
      return;
    }

    var regionList = <Region>[];

    for (var key in regions.keys) {
      regionList.add(Region(
        identifier: regions[key]!, 
        proximityUUID: key
      ));
    }

    if (_streamRanging != null) {
      if (_streamRanging!.isPaused) {
        _streamRanging?.resume();
        return;
      }
    }

    _streamRanging =
        flutterBeacon.ranging(regionList).listen((RangingResult result) {
      if (mounted) {
        setState(() {

          _regionBeacons[result.region] = result.beacons;
          for (var list in _regionBeacons.values) {
            for (var newBeacon in list) {
              var beaconIdx = _beacons.indexWhere((element) => _sameBeacon(element, newBeacon));
              if (beaconIdx != -1) {
                _beacons[beaconIdx] = newBeacon;
              }
              else {
                _beacons.add(newBeacon);
                beaconIds.add(newBeacon.minor);
              }

              if (newBeacon.minor == userBeaconId) {
                isUserSeat = newBeacon.accuracy <= acceptableAccuracy;
              }
            }
          }

          if (userBeaconId == -1 && beaconIds.isNotEmpty) {
            // print('Detected beacons: $beaconIds');
            userBeaconId = beaconIds[Random().nextInt(beaconIds.length)];
          }

          _beacons.sort(_compareParameters);
        });
      }
    });
  }

  pauseScanBeacon() async {
    _streamRanging?.pause();
    if (_beacons.isNotEmpty) {
      setState(() {
        _beacons.clear();
      });
    }
  }

  int _compareParameters(Beacon a, Beacon b) {
    int compare = a.proximityUUID.compareTo(b.proximityUUID);

    if (compare == 0) {
      compare = a.major.compareTo(b.major);
    }

    return compare;
  }

  bool _sameBeacon(Beacon lhs, Beacon rhs) {
    return lhs.proximityUUID == rhs.proximityUUID &&
      lhs.minor == rhs.minor && 
      lhs.major == rhs.major;
  }

  @override
  void dispose() {
    _streamRanging?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final idColor = isUserSeat ? Colors.lightGreen : Colors.red;
    final msg = isUserSeat ? 'You have your seat' : 'Please, take your seat';

    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              '$userBeaconId',
              style: TextStyle(
                fontSize: 32.0,
                color: idColor
              )
            ),
            Text(
              msg,
              style: const TextStyle(
                fontSize: 24.0,
              ),
            ),
          ],
        ),
      )
    );
  }
}